# Qlik Application Repository 
# results_default
### 
Created By XMO-NSIDE(Xavier Morosini) at Wed Apr 14 2021 17:04:53 GMT+0200 (Central European Summer Time)




Sheet Title | Description
------------ | -------------
Detailed recruitment data per depot|
Patient drop-out|
VAT & Custom fees|
Patient demand per site group|
Patient titrations & dose levels|
Supply plan - Future releases|
Welcome|
Risk of missed visit|
Detailed recruitment data per site group|
Per depot enrollment, randomization and completion|
Site group level enrollment, randomization and completion|
Quantities in stock - From initial state|
Forecasted site shipments|
Detailed patient demand data (#packages)|
Budget|
Shipment fulfilment analysis|
Planned shipments to depots|
Global patient enrollment, randomization and completion|
IRT settings|
Check release overlap|
Overage per package type|
Glossary|
Check remaining shelf lives|
Risk of missed dispensing and out of stock|
Global patient demand|
Number of depot shipments - Strategic estimation|
Inventory at depots|
Inventory at sites|
Forecasted depot supply|
Patient demand per depot|
Executive summary|



Branch Name|Qlik application
---|---
master|[https://qlik-dev-nov21.n-side.com/sense/app/f939c40e-e294-4d8d-8f15-8283cc8a6343](https://qlik-dev-nov21.n-side.com/sense/app/f939c40e-e294-4d8d-8f15-8283cc8a6343)
develop|[https://qlik-dev-feb19.n-side.com/sense/app/a7b63f84-625c-4388-9a5f-491f87c2a129](https://qlik-dev-feb19.n-side.com/sense/app/a7b63f84-625c-4388-9a5f-491f87c2a129)
advanced-users|[https://qlik-dev-feb19.n-side.com/sense/app/2137101b-be68-47ae-8201-18b808da40ee](https://qlik-dev-feb19.n-side.com/sense/app/2137101b-be68-47ae-8201-18b808da40ee)
advanced-users|[https://qlik-dev-feb19.n-side.com/sense/app/2137101b-be68-47ae-8201-18b808da40ee](https://qlik-dev-feb19.n-side.com/sense/app/2137101b-be68-47ae-8201-18b808da40ee)
results_5_2|[https://qlik-dev-feb19.n-side.com/sense/app/06dee4dc-632b-410f-9087-10959383652a](https://qlik-dev-feb19.n-side.com/sense/app/06dee4dc-632b-410f-9087-10959383652a)

Branch Name|Qlik application
---|---
reducedim|[https://qlik-dev-nov21.n-side.com/sense/app/1adca352-d999-4f91-a4b7-2aaeb2d1a807](https://qlik-dev-nov21.n-side.com/sense/app/1adca352-d999-4f91-a4b7-2aaeb2d1a807)